
#include <iostream>
#include <time.h>
#include <ctime> 

int main()
{
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << array[i][j] << ' ';
        }
        std::cout << '\n';
    }

    // ��� ��������� �������� ����� ��������� 
    struct tm buf;
    time_t t = time(0);
    tm* now = localtime_s (&buf, &t);
    int day = now->tm_mday;

    // ����� ����� ��������� � ������ ������� 
    int sum = 0;
    for (int j = 0; j < N; ++j) {
        sum += array[day % N][j];
    }
    std::cout << sum << day % N << ": " << sum << '\n';

    return 0;
}